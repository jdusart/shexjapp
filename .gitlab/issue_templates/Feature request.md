### Summary
(Summarize the requested feature concizely)

### Required behavior ?
(Describe the required behaviour.)

### Situation
(In which situation the new behaviour should be enabled)

### Current behavior ?
(Describe the current behaviour in this situation)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


/label ~feature ~needs-investigation
/cc @project-manager
