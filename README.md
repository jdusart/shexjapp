# ShapeDesigner


## Requirement

For simplicity, we recommand to use a jre with javafx. For the sun jre, it means using the version 8. For openjdk, you can use for example ZuluFX in version 8 or 11 (https://www.azul.com/downloads/zulu/zulufx/).

## Run

Launch it with the command:
java -jar shapedesigner-latest.jar


## Build

You need to install shape-generation https://github.com/jdusart/shape-generation

To compile with all the dependencies in one jar:
mvn clean compile assembly:single
The jar can be then found in the target directory.


## Tutorial

See the [doc](/doc/Index.md) or the [ISWC2019 demo video](https://nextcloud.univ-lille.fr/index.php/s/r3AYpogaEzeeycr).
