# Shape Designer User Documentation
(Under construction)

## Overview

Shape Designer **helps expert or novice users to construct a ShEx or SHACL** schema.
It is a GUI that integrate several useful tools:
*  highly parametrizable **schema extraction algorithm** that constructs shape constraints for an existing dataset
*  **schema edition** panel to adapt automatically extracted shape constraints or write your own schema
*  validation panel to **visualize the results of validation** of the data against your schema and inspect why validation failed

Shape Designer offers special support for the [Wikidata](https://www.wikidata.org) dataset

## Demo video

This demo was presented at the ISWC 2019 conference.
[See the demo video](https://nextcloud.univ-lille.fr-/index.php/s/r3AYpogaEzeeycr).
[See the demo paper](http://ceur-ws.org/Vol-2456/paper70.pdf).

## [Interface and main functionalities](Interface.md)

## [Patterns](Patterns-examples.md)
Where you learn how to use shape patterns to customize the schema extractoin algorithm.

## Wikidata use cases

We illustrate the use cases with _Authors_, that you can replace with any kind of entities present on Wikidata (Cities, Genes, Plant spiecies ...).

### [Discover the structure Wikidata entries](UC-wikidata-discover.md)

This use case might interest you if:
+ You consider **integrating information** about _Authors_ **from Wikidata** into your application
+ Your organisation has data about _Authors_ and is planning to **inject your data in Wikidata**
+ You want to discover the **basic Shape Designer use case** for Wikidata

### [Assert the quality of data](UC-wikidata-quality.md)

This use cas might intrest you if you are a **domain expert** on _Authors_ and you want to:
+ **Assert** the quality of _Author_ data on Wikidata
+ **Make corrections** in Wikidata on _Author_ entries with incomplete information

### [Retrieve the portion of the data needed for your application](UC-wikidata-retrieve-with-shex.md)

This use case might interest you if:
+ You are about to **integrate information** about _Authors_ **from Wikidata** into your application and you already have a clear idea of what kind of information you need
