# Use case : Use a ShEx schema to retrieve information from Wikidata

This describes a functionality of Shape Designer not yet implemented, but which we can be added fairly quickly.
Please contact us if you would find this useful.

Recall that we use _Authors_ in our examples, you can replace it with any kind of available Wikidata entries (Cities, Mouse genes, Programming languages, Invasive species, ...)

Assume we already have a ShEx schema that describes the properties of _Author_ in Wikidata that are interesting for our application. 
How to obtain such schema is described in [this use case](UC-wikidata-discover.md).

```
ShEx schema about author with part of the properties only (to come)
```

From this we can construct a SPARQL query that extracts preciely those properties described in the schema, as the one below

```
SPARQL query that extracts exactly the data described in the above schema (to come)
```

Shape Designer can automatically construct this query for you.
