# Learn shape patterns from examples

Patterns are in the heart of the schema extraction algorithm of Shape Designer. 
Every ShapeDesigner project comes with a set of predefined relevant patterns. 
You can also write your onw patterns to fit your own needs.

We present a series of shape pattern examples as well as their effect on schema extraction.

The syntax of shape patterns is based on ShEx compact syntax (ShExC), so basic knowledge about ShEx nd ShExC can be helpful.
You might also want to go through our [Quick introduction do ShEx](Shex-intro.md).

## Introduction

A pattern is applied to a set of sample nodes from the RDF graph. It defines the desired structure of the shape to be extracted for the sample nodes. 
Sets of sample nodes are defined by SPARQL queries, such as `SELECT ?item WHERE {?item a <http://ex.org/User>}`.

In the sequel we use these prefix definitions.

*Prefix definitions used in this section*
```
PREFIX ex:   <http://ex.org/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX p:    <http://ex.org/p/>
PREFIX xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

```

## Simple examples

We will use the following RDF [graph G1](#g1), presented in Turtle syntax, that contains information about users and the topics these users are interested in.

<a name="g1"><a>*Example graph G1*
```Turtle
ex:lod rdf:type ex:Topic ;
    p:name "Linked Open Data" ;
    p:relatedTo ex:semweb .
    
ex:semweb rdf:type ex:Topic ;
    p:name "Semantic Web" .
    
ex:alice rdf:type ex:User ;
    foaf:givenName  "Alice";
    foaf:familyName "Smith" ;
    p:interest ex:lod , ex:semweb .

ex:carol rdf:type ex:User ;
    foaf:givenName  "Carol";
    foaf:familyName "Sym" ;
    p:birthdate "1980-03-10"^^xsd:date ;
    p:interest ex:lod .
 
ex:john rdf:type ex:User ;
    foaf:givenName  "John";
    foaf:familyName "Doe" ;
    foaf:knows ex:carol, ex:alice ;
    p:interest ex:semweb .
```

We also use the above queries [Q_User](#Quser) and [Q_Topic](#Qtopic) that select nodes with types User and Topic, respectively. 

<a name="Quser"><a>*Query Q_User*
```SPARQL
SELECT ?item WHERE { ?item rdf:type ex:User. }
```

<a name="Qtopic"><a>*Query Q_Topic*
```SPARQL
SELECT ?item WHERE { ?item rdf:type ex:Topic. }
```

### A pattern for all properties and the type of their values

A pattern is always delimited by curly braces `{`and `}`.
The following [pattern P1](#p1) indicates that we are interested in all properties of the sample nodes, indicated by `~`, and the types or value kinds types of their values, indicated by `__`.

<a name="p1"></a>*Pattern P1*
```
{ ~ __ }
```

Below are the shapes resulting from the application of this pattern.

*ShEx shape obtained when [pattern P1](#p1) is applied to the sample defined by [query Q_Topic](#Qtopic) in [graph G1](#g1)*
```
{                                 # A node with type ex:Topic has
    rdf:type    IRI           ;   #   exactly one rdf:type that is an IRI
    p:name      xsd:string    ;   #   exactly one p:name that is a xsd:string
    p:relatedTo IRI         ?     #   an optional (?) p:relatedTo that is an IRI
}
```
The above shape indeed contains constraints for all three properties (`rdf:type`, `p:name` and `p:relatedTo`) that can appear in nodes with type Topic in [graph G1](#g1).
The shape contains a value constraint for the possible values of every property. 
This value constraint can be a literal datatype such as `xsd:string`, or a node kind such as `IRI`.
Finally, the constraint of each property indicates the possible cardinality of that property, where no cardinality is exactly one (as for `rdf:type`), and `?` indicates zero or one.

*ShEx shape obtained when [pattern P1](#p1) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                                      # A node with type ex:User has   
    rdf:type        IRI           ;    #   exactly one rdf:type that is an IRI
    foaf:givenName  xsd:string    ;    #   exactly one given name that is a string
    foaf:familyName xsd:string    ;    #   exactly one family name that is a string
    foaf:knows      IRI         * ;    #   0 or more (*) knows properties which values are IRIs
    p:interest      IRI         + ;    #   1 or more (+) interests that are IRIs 
    p:birthdate     xsd:date    ?      #   an optional birth date that is a date
}
```
Again, the above shape enumerates all properties that appear with the sample nodes (those with type User), the kind of their values, and the number of times (cardinality) each property can appear.
The `*` cardinality indicates 0 or more; the `+` cardinality indicates 1 or more.

### Filters for properties of interest

The following [pattern P2](#p2) indicates that we are interested only in properties with namespace `p`, indicated by `p:~`.

<a name="p2"></a>*Pattern P2*
```
{ p:~ __ }
```

The resulting shapes are as shown below. 
You can see that all properties that do not start with p: have been ommitted. 

*ShEx shape obtained when [pattern P2](#p2) is applied to the sample defined by [query Q_Topic](#Qtopic) in [graph G1](#g1)*
```
{                                
    p:name      xsd:string    ;  
    p:relatedTo IRI         ?    
}
```

*ShEx shape obtained when [pattern P2](#p2) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                                
    p:interest      IRI         + ;   
    p:birthdate     xsd:date    ?  
}
```

If we are interested in several namespaces, then we need to enumerate them, separated by `;`.

<a name="p3"></a>*Pattern P3*
```
{ p:~ __ ; rdf:~ __ }
```

*ShEx shape obtained when [pattern P3](#p3) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                                     
    rdf:type        IRI           ;   
    p:interest      IRI         + ;   
    p:birthdate     xsd:date    ?     
}
```

We can also use plain properties instead of their namespace only. In that case we have to omit the `~`, as in `foaf:givenName` below.

<a name="p4"></a>*Pattern P4*
```
{ p:~ __ ; foaf:givenName __ }

```
*ShEx shape obtained when [pattern P4](#p4) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                                     
    foaf:givenName  xsd:string    ;
    p:interest      IRI         + ;   
    p:birthdate     xsd:date    ?     
}
```

### Patterns for lists of values

Until now we saw patterns that always extract the kind or datatype of values of properties.
It is also possible to collect the actual values.

The following [pattern P5](#p5) indicates that we are interested in the list of possible values (indicated by `[__]`) for property `rdf:type`.

<a name="p5"></a>*Pattern P5*
```
{ rdf:type [__] }
```

*ShEx shape obtained when [pattern P5](#p5) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                
    rdf:type [ ex:User ]
}
```

The following [pattern P6](#p6) indicates that we are interested in the list of possible values for properties with namespace p:.

<a name="p6"></a>*Pattern P6*
```
{ p:~ [__] }
```

*ShEx shape obtained when [pattern P6](#p6) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{                                                
    p:interest  [ ex:lod ex:semweb ]       + ;    #    one or more p:interest properties which value is always among those listed 
    p:birthdate [ "1980-03-10"^^xsd:date ] ?      #    zero or one p:birthdate property which value is always the one listed
}
```

### Mixing property filters

The following [pattern P7](#p7) indicates that we are interested in the list of possible values of the property `p:interest` and the value kind of all other properties with namespace `p:`.
Remark that property `p:interest` matches both filters `p:~` and `p:interest`. When this is the case, the inference algorithm uses the most precise possible filter for that property.

<a name="p7"></a>*Pattern P7*
```
{ p:interest [__] ; p:~ __ }
```

Remark the difference compared with the results for [pattern P2](#p2) and [pattern P6](#p6).

*ShEx shape obtained when [pattern P7](#p7) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{
    p:interest [ ex:lod ex:semweb ] + ;
    p:birthdate xsd:date  ?  
}
```

In th following [pattern P8](#p8), we are interested in the lists of values of properties `rdf:type` and `p:interest`, and the value kind of all remaining properties (`~ __`).
The filter `~ __` is applied only for the properties for which there is no other more precise filter in the pattern. 

<a name="p8"></a>*Pattern P8*
```
{ rdf:type [__] ; p:interest [__] ; ~ __ }
```

*ShEx shape obtained when [pattern P8](#p8) is applied to the sample defined by [query Q_User](#Quser) in [graph G1](#g1)*
```
{
    rdf:type        [ ex:User ]            ;
    p:interest      [ ex:lod ex:semweb ] + ;
    foaf:givenName  xsd:string             ;  
    foaf:familyName xsd:string             ;  
    foaf:knows      IRI                  * ;  
    p:birthdate     xsd:date             ?   
}
```

### Patterns for namespaces of IRI values

Recall that `__` allows to construct a value kind or literal type constraint (such as IRI, Literal, Blank, xsd:string ...).
You can also construct a constraint with a list of values using `[__]`.
A third option is given by `__~` which is similar to `__` but provides a more precise constraint for IRIs by extracting their common namespace.

<a name="p9"></a>*Pattern P9*
```
{ ~ __~ }
```

*ShEx shape obtained when [pattern P9](#p9) is applied to the sample defined by [query Q_Topic](#Qtopic) in [graph G1](#g1)*
```
{                                 
    rdf:type    [ <http://ex.org/>~ ]   ;      # all values of rdf:type are IRI with common prefix <http://ex.org/>
    p:name      xsd:string              ;   
    p:relatedTo [ <http://ex.org/>~ ] ?        # similarly for all values of p:relatedTo
}
```

## Nested patterns

Sometimes shape constraints are defined not for the immediate neighbourhood of a node but for the neighbourhood of its neighbours, using a nested shape.
Nesting of patterns allows to indicate that we are interested in such shape.

For this section we are going to use the following prefix defintions, [graph G2](#g2) that contains information about places and images showing these places, and queries [Q_Place](#Qplace) and [Q_Image](#Qimage) selecting all nodes of the corresponding types.

*Prefix definitions used in this section*
```
PREFIX ex:    <http://ex.org/>
PREFIX p:     <http://ex.org/p/>
PREFIX geo:   <http://ex.org/geo/>
PREFIX im:    <http://ex.org/image/>
PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
```

<a name="g2"><a>*Example graph G2*
```Turtle
ex:pirin rdf:type ex:Place ;
    p:name "Pirin mountain" ;
    geo:coord [
        geo:long "23°30′" ;
        geo:lat  "43°30′"
    ] .

ex:todorka rdf:type ex:Place ;
    p:name "Mount Todorka" ;
    p:partOf ex:pirin .

ex:muratovo rdf:type ex:Place ;
    p:name "Muratovo Lake" ;
    p:near ex:todorka ;
    p:partOf ex:pirin ;
    geo:coord [
        geo:lat "41°44′45″N" ;
        geo:lon "23°24′22″E"
    ] .


ex:photo1 rdf:type ex:Image ;
    im:descr "Friends near Muratovo Lake" ;
    im:shows ex:todorka, ex:muratovo .
    
ex:photo2 rdf:type ex:Image ;
    im:descr "Pirin mountain from the north" ;
    im:shows ex:pirin , ex:todorka .

ex:photo3 rdf:type ex:Image ;
    im:shows ex:todorka .

```

<a name="Qplace"><a>*Query Q_Place*
```SPARQL
SELECT ?item WHERE { ?item rdf:type ex:Place. }
```

<a name="Qimage"><a>*Query Q_Image*
```SPARQL
SELECT ?item WHERE { ?item rdf:type ex:Image. }
```

Remark first that using [pattern P1](#p1) `{ ~ __ }` for places in graph G2 does not allow to capture that coordinates information is structured and contains two components for longitude and latitude respectively.

*ShEx shape obtained when [pattern P1](#p1) is applied to the sample defined by [query Q_Place](#Qplace) in [graph G2](#g2)*
```
{
    a IRI               ;
    p:name xsd:string   ;
    p:partOf IRI      ? ;
    p:near IRI        ? ;
    geo:coord BNODE   ?      # does not show that coordinates are structured  
}
```

A better shape can be obtained with the nested [pattern P10](#p10) below, which indicates that for the geo:coord property we require a nested shape describing the structure of the nodes that are values of that property.

<a name="p10"><a>*Pattern P10*
```
{ ~ __ ; geo:coord { ~ __ } }
```

*ShEx shape obtained when [pattern P10](#p10) is applied to the sample defined by [query Q_Place](#Qplace) in [graph G2](#g2)*
```
{
    a IRI               ;
    p:name xsd:string   ;
    p:partOf IRI      ? ;
    geo:coord {                   # nested pattern for the geo:coord property
        geo:lat xsd:string ;      #     whenever geo:coord is present, it always has one geo:lat that is a string
        geo:lon xsd:string ;      #     and one geo:lon that is a string as well
    } ? ;                         # cardinlity of the geo:coord property (0 or 1)
    p:near IRI ?
}
```

Remark that a nested pattern behaves as `__` whenever the corresponding values are literals.
The following [pattern P11](#p11) indicates that we are interested in property p:near only, and we want to extract a nested pattern for its values.
Because these values are all literals, a value constraint will be generated instead.

<a name="p11"><a>*Pattern P11*
```
{ 
    p:near { ~ __ } 
}
```

*ShEx shape obtained when [pattern P11](#p11) is applied to the sample defined by [query Q_Place](#Qplace) in [graph G2](#g2)*
```
{
    p:name xsd:string
}
```

A nested pattern can be used with property filters wherever `__` or `[__]` or `__~` can be used.
The following [pattern P12](#p12) indicates that we are interested in properties with namespace p: only, and we want a nested pattern for these properties.

<a name="p12"><a>*Pattern P12*
```
{ 
    p:~ { ~ __ } 
}
```

*ShEx shape obtained when [pattern P12](#p12) is applied to the sample defined by [query Q_Place](#Qplace) in [graph G2](#g2)*
```
{
    p:name xsd:string ;
    p:partOf {
        a IRI ;
        p:name xsd:string ;
        geo:coord BNODE
    } ? ;
    p:near {
        a IRI ;
        p:name xsd:string ;
        p:partOf IRI
    } ?
}
```

Similarly with nodes of type Image this time.

<a name="p13"><a>*Pattern P13*
```
{ 
    im:~ { ~ __ } 
}
```

*ShEx shape obtained when [pattern P13](#p13) is applied to the sample defined by [query Q_Image](#Qimage) in [graph G2](#g2)*
```
{
    im:shows {
        a IRI ;
        p:name xsd:string ;
        p:partOf IRI ? ;
        geo:coord BNODE ? ;
        p:near IRI ?
    } + ;
    im:descr xsd:string ?
}
```

Nested patterns are patterns themselves, so they can use filters as well.
The following [pattern P14](#p14) indicates that we want a nested pattern for im:shows which should contain only properties with namespace p: and their values, while for all the remaining properties we only require a constraint on their value (`~ __`).

<a name="p14"><a>*Pattern P14*
```
{ 
    im:shows {p:~ __} ; 
    ~ __ 
}
```

*ShEx shape obtained when [pattern P14](#p14) is applied to the sample defined by [query Q_Image](#Qimage) in [graph G2](#g2)*
```
{
    a IRI ;
    im:shows {
        p:name xsd:string ;
        p:partOf IRI ? ;
        p:near IRI ?
    } + ;
    im:descr xsd:string ?
}
```

Naturally, nested patterns can contain nested patterns themselves:

<a name="p15"><a>*Pattern P15*
```
{ 
    im:shows {
        geo:coord { ~ __ }
    } 
}
```

*ShEx shape obtained when [pattern P15](#p15) is applied to the sample defined by [query Q_Image](#Qimage) in [graph G2](#g2)*
```
{
    im:shows {
        geo:coord {
            geo:lat xsd:string ;
            geo:lon xsd:string
        } ?
    } +
}
```

We terminate with a complex pattern example that requires the list of values for rdf:type, a nested pattern for proprety im:shows that is only interested in properties with nemaspace geo: and p:, and is interested in particular in rdf:type of properties with namespace p:.

```
{
    rdf:type [__] ;
    im:shows {
        p:~ {
            rdf:type [__] ;
            ~ __ 
        }
        geo:~ { ~ __ }
    }
}
```

```
{
    a [ ex:Image ] ;
    im:shows {
        p:name xsd:string ;
        p:partOf {
            a [ ex:Place ] ;
            p:name xsd:string ;
            geo:coord BNODE
        } ? ;
        geo:coord {
            geo:lat xsd:string ;
            geo:lon xsd:string
        } ? ;
        # frequency: [3/5]
        p:near {
            a [ ex:Place ] ;
            p:name xsd:string ;
            p:partOf IRI
        } ?
    } +
}
```
