# Shape Designer interface and main functionalities
(under construction)

## Create a new project

At project creation we choose:
*  the schema language -- **ShEx** or **SHACL** -- used to create a schema
*  the RDF dataset to load among
   -  **RDF** : a local RDF file (.ttl, .n3 or any other format supported by RDF4J)
   -  **SparQL endpoint** : where the RDF dataset can be accessed
   -  **RDF4J SailRepository** : can be local on the disc
   -  **Wikidata** : uses the [Wikidata](http://www.wikidata.org) dataset

<details>
<summary>Show screenshot</summary>
![Menu for creating a project](images/new-project.png)
</details>
For the sequel we create a ShEx--RDF project with data file [Graph G1 from the Patterns tutorial](Patterns-examples.md#g1), available also [here](https://gitlab.inria.fr/jdusart/shexjapp/blob/master/examples/doc-examples/G1-users-topics.ttl).
</details>

## Schema creation view

ShapeDesigner loads the data and shows the schema creation view with these components
* **Patterns** panel : contains a list of patterns to be used for the extraction of shape constraints.
Every type of project comes with appropriate pre-defined patterns. 
You can add your own patterns.
You can learn more on patterns in the [Patterns tutorial](Patterns-examples.md)
* **Queries** panel : contains a list of SPARQL queries each of which selects a sets of nodes of the RDF dataset.
These nodes are used as samples for the extraction of shape constraints.
You can learn more on the role of the queries in the [Patterns tutorial](Patterns-examples.md)
* **Schema edition** panel : is where you edit the schema.
At start time it contains the prefix definitions extracted from the input RDF dataset together with some useful prefixes such as xsd: rdf: rdfs: etc.
The schema is initially empty.

<details>
<summary>Show screenshot</summary>
![Schema creation view](images/rdf-project.png)
</details>

## Extracting shape constraints

To extract a shape constraint we need a pattern and a query.
For now our queries list contains only an invalid example query.
We change it to select nodes with rdf:type ex:User.
Then we must **select one pattern, select one query, and Analyse**.

<details>
<summary>Show screenshot</summary>
![Ask for shape extraction](images/extract-shape-constraint.png)
</details>

The resulting automatically constructed shape constraint appears on the left:

<details>
<summary>Show screenshot</summary>
![Automatically extracted shape constraint](images/extracted-shape.png)
</details>

We can then choose to **Add shape** to the schema under construction.

<details>
<summary>Show screenshot</summary>
![Shape added to the schema](images/shape-added.png)
</details>

## Editing the schema

We can add other automatically extracted shapes by using different queries and/or patterns.
We add the shape for nodes with rdf:type ex:Topic, then give names to the shapes and clean the comments that were added by the shape extraction algorithm.

<details>
<summary>Show screenshot</summary>
![A schema with two shapes](images/two-shapes.png)
</details>

## Validation



